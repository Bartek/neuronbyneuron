import random
import numpy as np
import math

def nbn():
    input = [[-1, -1, -1, -1], [-1, -1, -1, 1], [-1, -1, 1, -1], [-1, -1, 1, 1],
             [-1,  1, -1, -1], [-1,  1, -1, 1], [-1,  1, 1, -1], [-1,  1, 1, 1],
             [ 1  -1  -1  -1], [ 1, -1, -1, 1], [ 1, -1, 1, -1], [ 1, -1, 1, 1],
             [ 1,  1, -1, -1], [ 1,  1, -1, 1], [ 1,  1, 1, -1], [ 1,  1, 1, 1]]

    output = [[1], [-1], [-1], [1],
             [-1],  [1],  [1],[-1],
             [-1],  [1],  [1],[-1],
              [1], [-1], [-1], [1]]



    topo = [5, 1, 2, 3, 4, 6, 1, 2, 3, 4, 5, 7, 1, 2, 3, 4, 5, 6];
    gain = [0.4, 0.4, 0.4];

    act = [2, 2, 2];

    param = [18, 3, 7, 4, 1, 16, 0.01, 1e+50, 1e-50, 1e-5, 500, 10];

    weight = generate_weights(param);
    iw = get_iw(topo);
    weight, iter, SSE = NBN_Train(input,output,topo,weight,param,iw, gain, act);


def NBN_Train(input,output,topo,weight,param,iw, gain, act):
    mu = param[6]
    m_I =  np.identity(param[0])
    TER = calculate_error(input, output, topo, weight, param, iw, gain, act)
    M_SSE = np.array([TER])

    m_weight = np.array(weight)

    for iter in range(1, param[10]):
        jw = 0
        gradient, hessian = calculate_gradient_Hessian(input,output,topo,weight,param,iw,gain,act)

        m_gradient = np.array(gradient)
        m_hessian = np.array(hessian)

        m_weight_backup = m_weight
        while 1:

            m_weight = m_weight_backup - ((m_hessian + mu * m_I)/ m_gradient).transpose()


            TER = calculate_error(input, output, topo, weight, param, iw, gain, act)

            # while iter>= M_SSE.shape[0]:
            #     np.append(M_SSE, 0)

            M_SSE[iter] = TER

            break

def calculate_gradient_Hessian(input, output, topo, weight, param, iw, gain, act):
    gradient = np.zeros((param[0], 1))
    hessian = np.zeros((param[0], param[0]))
    j = 0
    de = [0] * (param[3] + param[1])

    temp = []

    while (param[3] + param[1]) >= len(temp):
        temp.append(0)

    for p in range(param[5]):
        temp[0:param[3]] = input[p]



        for n in range(param[1]):
            j = param[3] + n


            net = weight[iw[n]]

            for i in range(iw[n] + 1, iw[n + 1]):
                # print(net, temp[topo[i] - 1], weight[i],topo[i], i, "\n")
                net += temp[topo[i] -1] * weight[i]

            temp[j], de[j] = actFuncDer(n -1,net,act,gain)


        for k in range(param[4]):
            # if(p == 6):
                # print(output[p][k], temp[param[1] + param[3] - param[4] + k])
            # print(temp, "\n")
            error = output[p][k] - temp[param[1] + param[3] - param[4] + k]

            J = np.zeros((1, param[0]))
            o = param[1] + param[3] - param[4] + k
            s = iw[o - param[3]]
            J[0][s] = -de[o]
            delo = np.zeros((1, param[1] + param[3] - param[4] + 1))

            for i in range(s + 1, iw[o + 1 - param[3]]):
                # print(s, i)
                # print(temp[ topo[ i ] -1], topo[ i ], J[ 0 ][ s ], "\n")
                J[0][i] = temp[topo[i] -1] * J[0][s]


                delo[0][topo[i] - 1] = delo[0][topo[i] -1] - weight[i] * J[0][s]

            for n in range(param[1] - param[4]):

                j = param[1] + param[3] - param[4] - n -1

                s = iw[j - param[3]]

                J[0][s] = -de[j] * delo[0][j]
                # print(de[j], delo[0][j], j, n, s, "\n")
                # print(J[0], "\n")
                for i in range(s + 1, iw[j - param[3] +1]):
                    # print(i, "\n")
                    # print(temp[topo[i] -1], topo[i], J[0][s], "\n")
                    J[0][i] = temp[topo[i] -1] * J[0][s]


                    delo[0][topo[i] - 1] = delo[0][topo[i] -1] - weight[i] * J[0][s]



            for a in range(len(gradient)):
                gradient[a] = gradient[a] + error * J[0][a]

            Jmat = [[0 for x in range(param[0])] for y in range(param[0])]

            for a in range(param[0]):
                for b in range(param[0]):
                    Jmat[a][b] = J[0][a] * J[0][b]


            for a in range(len(hessian)):
                for b in range(len(hessian[a])):
                    hessian[a][b] = hessian[a][b] + Jmat[a][b]

    print(hessian[17])
    return gradient, hessian


def calculate_error(input, output, topo, weight, param, iw, gain, act):
    err = 0;
    for p in range(param[5]):
        temp = input[p]

        for n in range(param[1]):
            j = param[3] + n
            # to jest jakieś dziwne net = weight(iw(n));
            net = weight[iw[n]]
            for i in range(iw[n] + 1, iw[n + 1] - 1):
                if topo[i] < len(temp):
                    net += temp[topo[i]] * weight[i]

            out = actFunc(n, net, act, gain)

            while j >= len(temp):
                temp.append(0.0)

            temp[j] = out

        for k in range(param[4]):
            err += (output[p][k] - temp[param[1] + param[3] - param[4]+k])**2

    return err


def actFunc(n,net,act,gain):
    if act[n] == 0:
        return gain[n]*net
    elif act[n] == 1:
        return 1 / (1 + math.exp(-gain[n] * net))
    elif act[n] == 2:
        return 2 / (1 + math.exp(-gain[n] * net)) -1
    elif act[n] == 3:
        return gain[n]*net/(1+gain[n]*abs(net))
    elif act[n] == 4:
        return 2*gain[n]*net/(1+gain[n]*abs(net))-1

def actFuncDer(n,net,act,gain):
    out = 0
    der = 0
    if act[n] == 0:
        out = gain[n] * net
        der = gain[n]
    elif act[n] == 1:
        out = 1 / (1 + math.exp(-gain[n] * net))
        der = gain[n] * (1 - out) * out
    elif act[n] == 2:
        out = 2 / (1 + math.exp(-gain[n] * net)) - 1
        der = gain[n] * (1 - out * out) / 2
    elif act[n] == 3:
        out = gain[n] * net / (1 + gain[n] * abs(net))
        der = gain[n] / (gain[n] * abs(net) + 1) ^ 2
    elif act[n] == 4:
        out = 2 * gain[n] * net / (1 + gain[n] * abs(net)) - 1
        der = 2 * gain[n] / (gain[n] * abs(net) + 1) ^ 2

    return out, der

def generate_weights(param):
    weight = []

    for i in range(param[0]):
        ra = 2 * random.random() - 1;
        while ra == 0:
            ra = 2 * random.random() - 1;

        weight.append(ra)

    return weight

def get_iw(topo):
    nmax = 0
    iw = []

    for i in range(len(topo)):
        if topo[i] > nmax:
            nmax = topo[i]
            iw.append(i)

    iw.append(i + 1)
    return iw


input = [[-1, -1, -1, -1], [-1, -1, -1, 1], [-1, -1, 1, -1], [-1, -1, 1, 1],
             [-1,  1, -1, -1], [-1,  1, -1, 1], [-1,  1, 1,  -1], [-1,  1, 1, 1],
             [ 1,  -1,  -1 , -1], [ 1, -1, -1, 1], [ 1, -1, 1, -1], [ 1, -1, 1, 1],
             [ 1,  1, -1, -1], [ 1,  1, -1, 1], [ 1,  1, 1, -1], [ 1,  1, 1, 1]]

output = [[1], [-1], [-1], [1],
             [-1],  [1],  [1],[-1],
             [-1],  [1],  [1],[-1],
              [1], [-1], [-1], [1]]



topo = [5, 1, 2, 3, 4, 6, 1, 2, 3, 4, 5, 7, 1, 2, 3, 4, 5, 6];
gain = [0.4, 0.4, 0.4];

act = [2, 2, 2];

param = [18, 3, 7, 4, 1, 16, 0.01, 1e+50, 1e-50, 1e-5, 500, 10];
weight = [0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5]

gradient,hessian =calculate_gradient_Hessian(input,output,topo,weight,param,get_iw(topo), gain, act);
# nbn()

# print(gradient)
